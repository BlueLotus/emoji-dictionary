//
//  EmojiDetailViewController.swift
//  Emoji Dictionary
//
//  Created by Carl on 2015/11/25.
//  Copyright © 2015年 Carl. All rights reserved.
//

import Foundation
import UIKit

class EmojiDetailViewController : UIViewController {
    
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var emojiDefinition: UILabel!
    
    var emoji = ""
    var emojiDefinitionValue = ""
    
    override func viewDidLoad() {
        self.emojiLabel.text = self.emoji
        self.emojiDefinition.text = self.emojiDefinitionValue
    }
    
}