//
//  EmojiListViewController.swift
//  Emoji Dictionary
//
//  Created by Carl on 2015/11/23.
//  Copyright © 2015年 Carl. All rights reserved.
//

import Foundation
import UIKit

class EmojiListViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var emoji = ""
    var emojiDefinitionValue = ""
    var emojis = ["🐹","🐶","🐱","🐭","🐰","🐯","🐸","🦁","🐷","🐧","🐺","🐗","🐢"];
    var emojiDefinitions = ["Hamster","Dog","Cat","Mouse","Rabbit","Tiger","Frog","Lion","Pig","Penguin","Wolf","Mountain Pig","Turtle",]
    
    override func viewDidLoad() {
        //How many cells are there in the table view
        self.tableView.dataSource = self
        //What's inside of each of the cells
        self.tableView.delegate = self
    }
    
    //How many rows are in the table view?
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.emojis.count
    }
    
    //What are going to be inside of the cells?
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell.textLabel!.text = self.emojis[indexPath.row] + "     " + self.emojiDefinitions[indexPath.row]
        return cell
    }
    
    //What happens when someone selects a row?
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.emoji = self.emojis[indexPath.row]
        self.emojiDefinitionValue = self.emojiDefinitions[indexPath.row]
        self.performSegueWithIdentifier("tableViewToEmojiSegue", sender: self)
    }
    
    //I'm about to perform a segue, is there anything you want me to do?
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var detailViewController = segue.destinationViewController as! EmojiDetailViewController
        detailViewController.emoji = self.emoji
        detailViewController.emojiDefinitionValue = self.emojiDefinitionValue
    }

}